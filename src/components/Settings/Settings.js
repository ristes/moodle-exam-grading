import React from 'react';
import AnswersRepository from "../../repository/AnswersRepository";

class Settings extends React.Component {

    uploadGrades = (e) => {
        const file = e.target.files[0];
        getFileContent(file).then(content => {
            AnswersRepository.uploadGrades(JSON.parse(content)[0]);
        });
    };

    uploadAnswers = (e) => {
        const file = e.target.files[0];
        getFileContent(file).then(content => {
            AnswersRepository.uploadAnswers(JSON.parse(content)[0]);
        });
    };

    uploadManualGrades = (e) => {
        const file = e.target.files[0];
        getFileContent(file).then(content => {
            const lines = content.split(/\r\n|\n/)
            const manualGrades = lines.map(l => {
                let parts = l.split(",");
                const result = {
                    index: parts[0].replace(/"/g, ""),
                    grade: parts[1].replace(/"/g, ""),
                    questionNumber: this.props.questionNumber
                };
                if (parts.length === 3) {
                    result['comment'] = parts[2].replace(/"/g, "");
                }
                return result;
            });
            AnswersRepository.uploadManualGrades(manualGrades);
        });
    };

    render() {
        let uploadForm = null;

        if (this.props.show) {
            uploadForm = (<>
                <div className="form-group">
                    <label className="col-form-label">
                        Grades (Choose grades exported from Moodle quiz with question grades):
                    </label>

                    <input
                        type="file"
                        className="form-control"
                        name='grades'
                        onChange={this.uploadGrades}/>

                </div>
                <div className="form-group">
                    <label className="col-form-label">Answers (Choose answers exported from Moodle responses with all
                        checkboxes set):</label>
                    <input
                        type="file"
                        className="form-control"
                        name='answers'
                        onChange={this.uploadAnswers}/>

                </div>
                <div className="form-group">
                    <label className="col-form-label">Manual Grades (exported from this app):</label>
                    <input
                        type="file"
                        className="form-control"
                        name='manualGrades'
                        onChange={this.uploadManualGrades}/>

                </div>
            </>)
        }


        return (<>
            <div className="pull-right">
                <button className="btn btn-primary btn-sm"
                        onClick={this.props.onToggleShow}>{this.props.show ? 'Hide' : 'Show upload form'}</button>
            </div>
            {uploadForm}
        </>);
    }
}


const getFileContent = (file) => {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
        reader.readAsText(file);
    });
}

export default Settings;