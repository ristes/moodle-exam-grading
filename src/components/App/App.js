import React, {Component, Fragment} from 'react';
import './App.css';
import Header from "../Header/header";
import AnswerRepository from "../../repository/AnswersRepository";
import {BrowserRouter as Router} from 'react-router-dom'
import Settings from "../Settings/Settings";
import {CSVLink} from "react-csv";


class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            answers: [],
            question: 0,
            filterDone: true,
            explanation: true,
            studentIndex: '',
            questionText: '',
            showSettings: true
        }
    }

    componentDidMount() {
        this.fetchData();
        const question = localStorage.getItem('question') || 0;
        console.log('question', question);
        this.setState({
            question: question
        });
    }


    render() {

        let actionsClass = 'col-10';
        let settingsClass = 'col-2';
        if (this.state.showSettings) {
            actionsClass = 'col-7';
            settingsClass = 'col-5';
        }

        return (
            <div className="App">
                <Router>
                    <Header/>
                    <div className="row mt-3 ml-3 mr-3">
                        <div className="col-md-12 ">
                            <div className="row">
                                <div className={actionsClass}>
                                    {this.actions()}
                                </div>
                                <div className={settingsClass}>
                                    <Settings show={this.state.showSettings}
                                              questionNumber={this.getQuestion()}
                                              onToggleShow={() => this.setState(state => {
                                                  return {showSettings: !state.showSettings};
                                              })}/>
                                </div>
                            </div>

                            <table className="table mt-3">
                                <tbody>
                                {this.display()}

                                </tbody>
                            </table>
                        </div>
                    </div>
                </Router>
            </div>
        );
    }


    fetchData = () => {
        let answers = AnswerRepository.showAnswers();
        this.setState({
            answers: answers,
            showSettings: !answers || answers.length === 0
        });
    }

    toggleChoice = (e) => {
        const name = e.target.name;
        let update = {};
        this.setState(state => {
            update[name] = !state[name];
            return update;
        });
    }

    changeFilterValue = (e) => {
        let update = {};
        update[e.target.name] = e.target.value;
        if (e.target.name === 'question') {
            console.log(e.target.value)
            localStorage.setItem('question', e.target.value);
        }
        this.setState(update);
    }

    updateGrade = (e) => {
        const newGrade = e.target.value;
        const index = e.target.name;
        this.setState({
            answers: AnswerRepository.updateGrade(index, this.getQuestion(), newGrade)
        });
    }

    updateGrade = (e) => {
        const grade = e.target.value;
        const index = e.target.name;
        this.setState({
            answers: AnswerRepository.updateGrade(index, this.getQuestion(), grade)
        });
    }


    updateComment = (e) => {
        const comment = e.target.value;
        const index = e.target.name;
        this.setState({
            answers: AnswerRepository.updateComment(index, this.getQuestion(), comment)
        });
    }


    setDone = (e) => {
        const index = e.target.name;
        this.setState({
            answers: AnswerRepository.setDone(index, this.getQuestion())
        });
    }


    getQuestion = () => {
        return this.state.question || 0;
    }

    finishedGrades = () => {
        return this.state.answers.filter(a => a.questions[this.getQuestion()].done)
            .map(a => [a.index, a.questions[this.getQuestion()].grade, a.questions[this.getQuestion()].comment || '']);
    }

    actions = () => {
        let answersSelect = null;
        if (this.state.answers && this.state.answers.length > 0) {
            answersSelect = (
                <select value={this.state.question}
                        onChange={this.changeFilterValue}
                        name="question"
                        className="form-control">
                    {this.state.answers[0].questions.map((q, ix) => (<option key={ix} value={ix}>{ix + 1}</option>))}
                </select>
            );
        }

        return (
            <><h1>Still to
                grade: {this.state.answers.filter(a => !a.questions[this.getQuestion()].done).length} </h1>
                <div className="row">
                    <div className="col-1">
                        <label className="col-form-label"> Question#:</label>
                    </div>
                    <div className="col-1">
                        {answersSelect}
                    </div>
                    <div className="col-2">
                        <label className="col-form-label">Hide done?</label>
                        <input type="checkbox" className="ml-2" name="filterDone"
                               onChange={this.toggleChoice}
                               checked={this.state.filterDone}/>
                    </div>
                    <div className="col-3">
                        <label className="col-form-label">Show explanation?</label>
                        <input type="checkbox" className="ml-2" name="explanation"
                               onChange={this.toggleChoice}
                               checked={this.state.explanation}/>
                    </div>
                    <div className="col-2">
                        <input type="number"
                               name="studentIndex"
                               title="Filter by student index"
                               placeholder="Student index"
                               className="form-control"
                               onChange={this.changeFilterValue}
                               value={this.state.studentIndex}/>

                    </div>

                    <div className="col-3">
                        <input type="text"
                               name="questionText"
                               title="Filter by question text"
                               placeholder="Question text"
                               className="form-control"
                               onChange={this.changeFilterValue}
                               value={this.state.questionText}/>

                    </div>


                </div>

                <div>
                    <CSVLink className="btn btn-primary btn-sm mt-3"
                             filename={'grades-q' + this.getQuestion() + '.csv'}
                             data={this.finishedGrades()}>Download finished</CSVLink>
                </div>
            </>
        );
    }

    display = () => {
        let answers = this.state.answers;

        if (this.state.filterDone) {
            answers = this.state.answers.filter(a => !a.questions[this.getQuestion()].done);
        }
        if (this.state.studentIndex) {
            answers = answers.filter(a => a.index.indexOf(this.state.studentIndex) >= 0);
        }
        if (this.state.questionText) {
            answers = answers.filter(a => a.questions[this.getQuestion()].question.indexOf(this.state.questionText) >= 0);
        }

        return answers
            .sort((x, y) => {
                const x1 = x.questions[this.getQuestion()].question;
                const y1 = y.questions[this.getQuestion()].question;
                if (x1 === y1) {
                    return 0;
                } else if (x1 > y1) {
                    return 1;
                } else {
                    return -1;
                }
            })
            .map(this.renderStudentQuestionResponse);
    }

    renderStudentQuestionResponse = (a, ix) => {

        const question = a.questions[this.getQuestion()];
        const key = a.index + "_" + ix;
        let highlightReason = "";
        if (ix === 0) {
            highlightReason = "reason-highlight";
        }
        return (<Fragment key={key}>
            <tr>
                <th rowSpan="3" style={{"minWidth": "250px"}}>

                    <input type="text"
                           title="Question Points (Any change overrides the original value)"
                           className="form-control"
                           name={a.index}
                           autoFocus={ix === 0}
                           value={question.grade}
                           onChange={this.updateGrade}
                    />
                    <input type="text"
                           title="Comment about the change"
                           className="form-control"
                           placeholder="Comment"
                           name={a.index}
                           value={question.comment || ''}
                           onChange={this.updateComment}
                    />
                    <button className="btn btn-primary btn-xs"
                            name={a.index}
                            onClick={this.setDone}>
                        Done
                    </button>
                    <div>
                        ({a.index}) {a.name} {a.lastName}
                    </div>
                </th>
                <th colSpan="2" title="Question text">{question.question}</th>
            </tr>
            <tr>
                <td title="Answered">{question.answer}</td>
                <td title="Correct" className="text-success">{question.right}</td>
            </tr>
            <tr>
                <td colSpan="2" className={"pt-4 pb-4 bg-light " + highlightReason} title="Explanation">
                    {this.state.explanation && question.reason}
                </td>
            </tr>
        </Fragment>);
    }
}

export default App;
