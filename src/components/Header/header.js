import React from 'react';
const Header = (props) => {
    return (
        <header>
            <nav className="navbar navbar-expand-md navbar-dark navbar-fixed bg-dark">
                <a className="navbar-brand" href="#">Moodle Exam grading</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                        aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarCollapse">
                    <ul className="navbar-nav mr-auto">
                        {/*<li className="nav-item">*/}
                        {/*    <Link className="nav-link" to={"/documents"}>Grade</Link>*/}
                        {/*</li>*/}
                    </ul>

                </div>
            </nav>
        </header>
    )
}
export default Header;