class AnswerRepository {

    constructor() {
        const stored = localStorage.getItem('answers');
        if (stored) {
            this.answers = JSON.parse(stored);
        } else {
            this.answers = [];
        }
    }

    uploadGrades = (grades) => {
        localStorage.setItem('grades', JSON.stringify(grades));
    }


    uploadAnswers = (answers) => {
        let storedGrades = localStorage.getItem('grades');
        if (!storedGrades) {
            throw Error('Upload grades first');
        }
        const grades = JSON.parse(storedGrades);

        localStorage.setItem('answers', JSON.stringify(this.processSeparateReasonAnswers(answers, grades)));
        window.location.reload();
    }


    showAnswers = () => {
        return this.answers;
    }

    updateGrade = (index, question, grade) => {
        const student = this.findStudent(index);
        student.questions[question].grade = grade;
        return this.saveAnswers();
    }

    updateComment = (index, question, comment) => {
        const student = this.findStudent(index);
        student.questions[question].comment = comment;
        return this.saveAnswers();
    }


    setDone = (index, question) => {
        const student = this.findStudent(index);
        student.questions[question].done = true;
        student.questions[question].true = true;
        return this.saveAnswers();
    }

    findStudent = (index) => {
        const ix = this.answers.findIndex(a => a.index === index);
        return this.answers[ix];
    }

    saveAnswers = () => {
        localStorage.setItem('answers', JSON.stringify(this.answers));
        this.answers = JSON.parse(localStorage.getItem('answers'));
        return this.answers;
    }

    uploadManualGrades = (manualGrades) => {
        for (let i = 0; i < manualGrades.length; i++) {
            let grade = manualGrades[i];

            let student = this.findStudent(grade.index);
            if (student) {
                student.questions[grade.questionNumber].done = true;
                student.questions[grade.questionNumber].grade = grade.grade;
                student.questions[grade.questionNumber].manual = true;
                student.questions[grade.questionNumber].comment = grade.comment || "";
            } else {
                console.log("invalid", grade)
            }
        }
        this.saveAnswers();
        window.location.reload();
    }

    processSeparateReasonAnswers = (answers, grades) => {
        return answers.map((a, ix) => {
            const ans = {
                name: a[0],
                lastName: a[1],
                index: a[2],
                points: a[8],
                questions: []
            };
            const studentGrade = grades.find(gg => gg[2] === ans.index);
            for (let i = 9; i < a.length; i += 3) {
                const answer = a[i + 1];
                const right = a[i + 2];
                let grade = studentGrade[(i - 9) / 3 + 9]

                if (right === '-') {
                    const quest = ans.questions[ans.questions.length - 1];
                    quest.reason = answer;
                    if (answer === '-') {
                        quest.done = true;
                        quest.comment = 'No explanation';
                        quest.grade = Math.min(quest.grade, 0);
                    } else {
                        quest.comment = null;
                        quest.done = false;
                    }
                } else {
                    ans.questions.push(this.getQuestionData(a[i], answer, right, grade));
                }

            }
            ans.questions.forEach(q => {
                if (q.comment) {
                    q.grade = Math.min(q.grade, 0);
                    q.done = true;
                }
            });
            return ans;
        });
    }


    getQuestionData = (text, answer, right, grade) => {
        const hasReason = answer.indexOf(' | Reason:') > 0;
        grade = grade === '-' ? 0 : grade;
        const ar = answer.split(' | Reason:')
        const result = {
            question: text,
            answer: ar[0],
            right: right,
            grade: grade,
            comment: null,
            done: true
        };

        if (ar.length === 2) {
            result.reason = ar[1];
            if (result.reason) {
                result.done = false;
            } else {
                result.comment = 'No explanation';
            }
        } else {
            result.comment = 'No explanation';
        }

        return result;

    }
}

export default new AnswerRepository();