import axios from 'axios';

const instance = axios.create({
    // baseURL: 'http://localhost:5000',
    baseURL: 'http://wp.finki.ukim.mk/fbw/',
    headers: {
        'Access-Control-Allow-Origin': '*'
    },
});

export default instance;